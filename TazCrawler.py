from bs4 import BeautifulSoup
import requests
import pandas as pd

baseURL = 'https://taz.de'
resort = 'https://taz.de/Berlin/!p4649/'
articleURL = []

""""
Get a soup contruct for a resort
IN: resort = URL of one resort for example sports
OUT: soup = readable html
"""
def getSoupConstruction(resort):
    resort = requests.get(resort)
    soup = BeautifulSoup(resort.content, 'html.parser')
    return soup

"""
Crawling all article for a resort
IN: soup = readable html
OUT: articleURL = list of all arcticle for a resort
"""
def crawlingArticlesURL(soup):
    for a in soup.find_all('a', href=True, role='link'):
        articleURL.append(baseURL+a['href'])
    return articleURL

"""
Get titel and subtitle of an article
IN: articleURL = URL of an specific article
OUT: headline = title and subtitle of the article
"""
def getTitelAndSubtitle(articleURL):
    soup = getSoupConstruction(articleURL)
    headline = (soup.find('h1').get_text())
    return headline

"""
Get intro of an article
IN: articleURL = URL of an specific article
OUT: intro = summary of an article
"""
def getIntro(articleURL):
    soup = getSoupConstruction(articleURL)
    intro = (soup.find("p", class_="intro").get_text())
    return intro

"""
Get author of an article
IN: articleURL = URL of an specific article
OUT: author = creator of an article
"""
def getAuthor(articleURL):
    soup = getSoupConstruction(articleURL)
    author = (soup.find('h4').get_text())
    return author

"""
Get publish date of an article
IN: articleURL = URL of an specific article
OUT: date = date when the article was published
"""
def getDate(articleURL):
    soup = getSoupConstruction(articleURL)
    date = (soup.find("li", class_="date").get_text())
    return date

"""
Get text of an article
IN: articleURL = URL of an specific article
OUT: body = text of the article
"""
def getBody(articleURL):
    soup = getSoupConstruction(articleURL)
    resort = (soup.find("article", class_="sectbody").get_text())
    return resort


#Tests for above methods
test = getSoupConstruction(resort)
result = crawlingArticlesURL(test)
print(result[0])
testTitle = getTitelAndSubtitle(result[0])
testIntro = getIntro(result[0])
testAuthor = getAuthor(result[0])
testDate = getDate(result[0])
testBody = getBody(result[0])
print(testTitle)
print(testIntro)
print(testAuthor)
print(testDate)
print(testBody)

